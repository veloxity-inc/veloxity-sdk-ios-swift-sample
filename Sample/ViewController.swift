//
//  ViewController.swift
//  Sample
//
//  Created by Cemal YILMAZ on 19.12.2019.
//  Copyright © 2019 Veloxity. All rights reserved.
//

import UIKit
import VeloxitySDK

class ViewController: UIViewController {
    // Documentation: 3.7
    @IBAction func sendCustomData() {
        Veloxity.sharedInstance().sendCustomData(["customDataKey": "customDataValue"])
    }
}
