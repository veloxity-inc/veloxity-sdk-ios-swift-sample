//
//  SettingsTableViewCell.swift
//  Sample
//
//  Created by Cemal YILMAZ on 20.12.2019.
//  Copyright © 2019 Veloxity. All rights reserved.
//

import UIKit
import VeloxitySDK

class SettingsTableViewCell: UITableViewCell  {
    @IBOutlet weak var switch_service: UISwitch?

    override func awakeFromNib() {
        super.awakeFromNib()
        self.serviceStateControl()
    }

    func serviceStateControl() {
        if Veloxity.sharedInstance()?.serviceStatus() == true {
            switch_service?.setOn(true, animated: false)
        } else {
            switch_service?.setOn(false, animated: false)
        }
    }
    
    func changeUserDataUsageStatus(status: Bool) {
        self.switch_service?.setOn(status, animated: false)
    }
    
    @IBAction func switchChanged(sender: Any?) {
        if self.switch_service?.isOn == true {
            Veloxity.sharedInstance().setLicenseKey("<LICENSE_KEY>")
            Veloxity.sharedInstance().setWebServiceUrl("<WEB_SERVICE_URL>")

            Veloxity.sharedInstance().setAuthorizationMenu(NSLocalizedString("data_usage_title", comment: "Data Usage Permission"), withMessage: NSLocalizedString("data_usage_message", comment: "Data Usage Message"), andAcceptTitle: "Accept", andDenyTitle: "Deny")
            Veloxity.sharedInstance().delegate = self
            Veloxity.sharedInstance()?.optIn()
            
        } else {
            Veloxity.sharedInstance()?.optOut()
        }
    }
}

extension SettingsTableViewCell : VeloxityDelegate {
    func vlxAuthorizationDidSucceed() {
        print("VeloxitySDKApp - Authorization Did Succeed")
        self.changeUserDataUsageStatus(status: true)
    }
    func vlxAuthorizationDidFailed() {
        print("VeloxitySDKApp - Authorization Did Failed")
        self.changeUserDataUsageStatus(status: false)
    }
}
