//
//  AppDelegate.swift
//  Sample
//
//  Created by Cemal YILMAZ on 19.12.2019.
//  Copyright © 2019 Veloxity. All rights reserved.
//

import UIKit
import VeloxitySDK

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, VeloxityDelegate {
    
    var window: UIWindow?

    internal func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        Veloxity.sharedInstance().setLicenseKey("<LICENSE_KEY>")
        Veloxity.sharedInstance().setWebServiceUrl("<WEB_SERVICE_URL>")
        
        // Documentation: 3.4.2
        // comment out below line if you want no dialog from Veloxity SDK
        Veloxity.sharedInstance().setAuthorizationMenu(NSLocalizedString("data_usage_title", comment: "Data Usage Permission"),
                                                       withMessage: NSLocalizedString("data_usage_message", comment: "Data Usage Message"),
                                                       andAcceptTitle: "Accept",
                                                       andDenyTitle: "Deny")
        
        // Documentation: 3.4.3
        Veloxity.sharedInstance().setNeverAskPermissions(true)
        
        // New method
        // to enable VeloxitySDK to respect ATT (Application Tracking Transperency) preference of user
        // If you want Veloxity to track user data regarding ATT pref. you should call the method with 'TRUE'
        // If your application does required to use ATT, call the method with 'false'
        // Default value is 'false'
        Veloxity.sharedInstance().setUseATTPermissions(true)
        
        Veloxity.sharedInstance().delegate = self
        Veloxity.sharedInstance().start()
        
        
    
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
            (granted, error) in
        }
        
        if let userInfo:[AnyHashable: Any] = launchOptions?[UIApplication.LaunchOptionsKey.remoteNotification] as? [AnyHashable : Any] {
            Veloxity.sharedInstance()?.startBackgroundTransaction(userInfo: userInfo)
        }
        
        UIApplication.shared.registerForRemoteNotifications()
      
        return true
    }

    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        // register device token to VeloxityServers.
        // Documentation: 4.2
        Veloxity.sharedInstance().registerDeviceToken(deviceToken)
    }

    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print(error)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        // Documentation: 4.3
        // Process message on Veloxity

        Veloxity.sharedInstance().startBackgroundTransaction(userInfo: userInfo)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // Documentation: 4.3
        // Process message on Veloxity
        
        Veloxity.sharedInstance().startBackgroundTransaction(userInfo: userInfo)
        completionHandler(.newData)
    }

    func userNotificationCenter(_ center: UNUserNotificationCenter,  willPresent notification: UNNotification, withCompletionHandler   completionHandler: @escaping (_ options:   UNNotificationPresentationOptions) -> Void) {
        // Documentation: 4.3
        // Process message on Veloxity
        
        let userInfo = notification.request.content.userInfo as? [AnyHashable: Any]
        Veloxity.sharedInstance()?.startBackgroundTransaction(userInfo: userInfo)
     }

    func vlxAuthorizationDidSucceed() {
        // VeloxitySDK initialization completed successfully.
        print("Veloxity vlxAuthorizationDidSucceed");
        print("ServiceStatus:>> \(Veloxity.sharedInstance().serviceStatus())")

    }
        
    func vlxAuthorizationDidFailed() {
        // VeloxitySDK initialization Failed.
        print("Veloxity vlxAuthorizationDidFailed");

        print("ServiceStatus:>> \(Veloxity.sharedInstance().serviceStatus())")

    }
}
