//
//  SettingsViewController.swift
//  Sample
//
//  Created by Cemal YILMAZ on 20.12.2019.
//  Copyright © 2019 Veloxity. All rights reserved.
//

import UIKit

class SettingsTableViewController: UITableViewController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Veloxity Service Status"
    }
    
    override func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        return "You can enable or disable the service state of Veloxity SDK"
    }
}
