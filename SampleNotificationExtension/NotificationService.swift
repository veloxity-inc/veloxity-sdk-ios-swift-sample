//
//  NotificationService.swift
//  SampleNotificationExtension
//
//  Created by Cemal YILMAZ on 26.12.2019.
//  Copyright © 2019 Veloxity. All rights reserved.
//

import UserNotifications
import VeloxitySDK

class NotificationService: UNNotificationServiceExtension {
    var contentHandler: ((UNNotificationContent) -> Void)?
    var receivedRequest: UNNotificationRequest!
    var bestAttemptContent: UNMutableNotificationContent?
    
    override func didReceive(_ request: UNNotificationRequest, withContentHandler contentHandler: @escaping (UNNotificationContent) -> Void) {
        self.receivedRequest = request;
        self.contentHandler = contentHandler
        bestAttemptContent = (request.content.mutableCopy() as? UNMutableNotificationContent)
        
        if let bestAttemptContent = bestAttemptContent {
            Veloxity.sharedInstance()?.didReceiveNotificationExtensionRequest(self.receivedRequest,
                                                                              with: bestAttemptContent,
                                                                              completion: { (content) in
                                                                                contentHandler(content!)
            })
        }
    }
    
    override func serviceExtensionTimeWillExpire() {
        if let contentHandler = contentHandler, let bestAttemptContent =  bestAttemptContent {
            contentHandler(bestAttemptContent)
        }
    }
}
